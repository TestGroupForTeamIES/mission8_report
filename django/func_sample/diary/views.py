from django.shortcuts import render, redirect, get_object_or_404
from .forms import DayCreateForm
from .models import Day
# from django.http import HttpResponse

def index(request):
    # aboutページ
    context = {
        'day_list':Day.objects.all(),
    }
    return render(request, 'diary/day_list.html',context)

def add(request):

    # 送信内容元にフォームを作成する。POSTじゃなければ秋のフォームで送る
    form = DayCreateForm(request.POST or None)

    # method=POST（送信ボタン押下時に入力内容が問題ない場合に実行）
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('diary:index')
    # 通常のページアクセス
    context = {
        'form':form
    }
    return render(request, 'diary/day_form.html', context)

def update(request,pk):

    # urlのPを基に、Dayを取得
    day = get_object_or_404(Day, pk=pk)

    # 送信内容元にフォームを作成する。POSTじゃなければ秋のフォームで送る
    form = DayCreateForm(request.POST or None, instance=day)

    # method=POST（送信ボタン押下時に入力内容が問題ない場合に実行）
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('diary:index')

    # 通常のページアクセス
    context = {
        'form':form
    }
    return render(request, 'diary/day_form.html', context)

def delete(request,pk):

    # urlのPを基に、Dayを取得
    day = get_object_or_404(Day, pk=pk)

    # method=POST（送信ボタン押下時に入力内容が問題ない場合に実行）
    if request.method == 'POST':
        day.delete()
        return redirect('diary:index')

    # 通常のページアクセス
    context = {
        'day':day,
    }
    return render(request, 'diary/day_config_delete.html', context)

def detal(request,pk):

    # urlのPを基に、Dayを取得
    day = get_object_or_404(Day, pk=pk)

    # 通常のページアクセス
    context = {
        'day':day,
    }
    return render(request, 'diary/day_detal.html', context)