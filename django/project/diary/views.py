from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import DayCreateForm
from .models import Day
from django.urls import reverse_lazy
from django.views import generic
# from django.http import HttpResponse


class IndexView(generic.ListView):
    model = Day
    paginate_by = 3

class AddView(LoginRequiredMixin, generic.CreateView):
    model = Day
    form_class = DayCreateForm
    #fields = '__all__'
    success_url =reverse_lazy('diary:index') #/diary/

class UpdateView(LoginRequiredMixin, generic.UpdateView):
    model = Day
    form_class = DayCreateForm
    #fields = '__all__'
    success_url =reverse_lazy('diary:index') #/diary/

class DeleteView(LoginRequiredMixin, generic.DeleteView):
    model = Day
    success_url =reverse_lazy('diary:index') #/diary/

class DetailView(generic.DetailView):
    model = Day

"""
def index(request):
    # aboutページ
    context = {
        'day_list':Day.objects.all(),
    }
    return render(request, 'diary/day_list.html',context)

def add(request):

    # 送信内容元にフォームを作成する。POSTじゃなければ秋のフォームで送る
    form = DayCreateForm(request.POST or None)

    # method=POST（送信ボタン押下時に入力内容が問題ない場合に実行）
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('diary:index')
    # 通常のページアクセス
    context = {
        'form':form
    }
    return render(request, 'diary/day_form.html', context)

def update(request,pk):

    # urlのPを基に、Dayを取得
    day = get_object_or_404(Day, pk=pk)

    # 送信内容元にフォームを作成する。POSTじゃなければ秋のフォームで送る
    form = DayCreateForm(request.POST or None, instance=day)

    # method=POST（送信ボタン押下時に入力内容が問題ない場合に実行）
    if request.method == 'POST' and form.is_valid():
        form.save()
        return redirect('diary:index')

    # 通常のページアクセス
    context = {
        'form':form
    }
    return render(request, 'diary/day_form.html', context)



def delete(request,pk):

    # urlのPを基に、Dayを取得
    day = get_object_or_404(Day, pk=pk)

    # method=POST（送信ボタン押下時に入力内容が問題ない場合に実行）
    if request.method == 'POST':
        day.delete()
        return redirect('diary:index')

    # 通常のページアクセス
    context = {
        'day':day,
    }
    return render(request, 'diary/day_config_delete.html', context)

def detal(request,pk):

    # urlのPを基に、Dayを取得
    day = get_object_or_404(Day, pk=pk)

    # 通常のページアクセス
    context = {
        'day':day,
    }
    return render(request, 'diary/day_detal.html', context)


"""
