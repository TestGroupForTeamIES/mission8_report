from django.urls import path
from . import views

app_name = 'diary'

urlpatterns = [

    path('', views.IndexView.as_view(), name='index'), # /diary
    path('add/', views.AddView.as_view(), name='add'), # /diary
    path('update/<int:pk>/', views.UpdateView.as_view(), name='update'),  # /diary
    path('delete/<int:pk>/', views.DeleteView.as_view(), name='delete'),  # /diary
    path('detail/<int:pk>/', views.DetailView.as_view(), name='detail'),  # /diary


# path('', views.index, name='index'), # /diary
# path('add/', views.add, name='add'), # /diary/add
# path('update/<int:pk>/', views.update, name='update'),  # /diary/update/整数
# path('delete/<int:pk>/', views.delete, name='delete'),  # /diasry/delete/整数
# path('detal/<int:pk>/', views.detal, name='detal'),  # /diary/detal/整数


]
