from django.db import models

# django では, datetime.nowの変わりのtimezone.nowで現在の時間を示す
from django.utils import timezone


class Day(models.Model):
    title = models.CharField('タイトル', max_length=200)
    text = models.TextField('本文')
    date = models.DateTimeField('日付', default=timezone.now)

    def __str__(self):
        # return self.text
        # return self.date
        return self.title
