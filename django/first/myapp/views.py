from django.shortcuts import render
# from django.http import HttpResponse

def index(request):
    # トップページ
    context ={
        'name':'Chueh-Han Lo',
    }
    return render(request, 'myapp/index.html',context )

def about(request):
    # aboutページ
    return render(request, 'myapp/about.html')

def info(request):
    # infoページ
    return render(request, 'myapp/info.html')