from django.urls import path
from . import views

app_name = 'myapp'

urlpatterns = [
    # ここで新しいページを追加
    path('', views.index, name='index'), # 127.0.0.1:8080/myapp
    path('about', views.about, name='about'),
    path('info', views.info, name='info'),
]
