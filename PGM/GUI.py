# bs4
from bs4 import BeautifulSoup

# requests
import requests

res = requests.get('https://torina.top')
# res = requests.get('https://www.google.com')
# print(res.text)
# print('')

soup = BeautifulSoup(res.text, 'html.parser')
for h2 in soup.find_all('h1'):
    print(h2.text)
