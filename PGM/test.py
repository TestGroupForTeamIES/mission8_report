# shutil
import shutil

# ファイルのコピー
shutil.copy('test.py', 'test_copy.py')
# フォルダのコピー
shutil.copytree('bk', 'bk2')
shutil.copytree('bk', 'bk3')
# ファイル名の変更、ファイルの移動
shutil.move('test_copy.py',"bk3/test_copy2.py")
# DIRを削除する
shutil.rmtree('bk2')

# os ライブラリ
import os
# ファイルを削除
os.remove("bk3/test_copy2.py")

# DIRを削除
shutil.rmtree('bk3')
