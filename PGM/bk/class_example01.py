# クラス
class Student:
    # __init__：クラスにオブジェを作成する関数
    def __init__(self,name):
        # self関数自身のオブジェ
        self.name = name
        self.score = {} # 空辞書
        
    # 成績を追加するメソッド
    def add_score(self, sub_name, point):
        self.score[sub_name] = point

    # 成績を読み込むメソッド
    def get_score(self, sub_name):
        return self.score.get(sub_name,'見つからない項目')

members ={}
members['Alex']=Student('Alex')
members['Tom']=Student('Tom')
members['Alex'].add_score('TOEIC',550)
members['Tom'].add_score('TOEIC',650)
members['Alex'].add_score('TOFEL',70)
members['Tom'].add_score('TOFEL',80)

# 名前を抽出
for name in members:
    # 科目を抽出
    for subject in members[name].score:
        # 成績を抽出
        score = members[name].get_score(subject)
        print('{0}の{1}成績は{2}'.format(name,subject,score))
