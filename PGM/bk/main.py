# モジュール

# subモジュールをインポット
import sub

a = sub.add(2,3)
print(a)

# sub2モジュールをインポット
from sub2 import addddd

b = addddd(4,7)
print(b)

# subモジュールをインポット
import sub2 as eee

c = eee.subbbb(0,3)
print(c)

# subモジュールをインポット
from sub2 import *

d = addddd(4,9)
e = subbbb(d,7)
print(e)
